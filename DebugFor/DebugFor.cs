﻿using System;

namespace DebugFor
{
    class DebugFor
    {
        static void Main(string[] args)
        {
            var numero1 = 548745184ul;
            var numero2 = 25145ul;
            ulong result = 0ul;
            for (ulong i = 0ul; i < numero2; i++)
            {
                result += numero1;
            }
            Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);
            Console.Read();
        }
    }
}

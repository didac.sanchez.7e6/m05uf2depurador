﻿using System;

namespace TeamColor
{
    class TeamColor
    {
        static void Main()
        {
            string color1 = Console.ReadLine();
            string color2 = Console.ReadLine();
            color1 = color1.ToLower();
            color2 = color2.ToLower();
            if (color1 == "white")
            {
                if (color2 == "green") Console.WriteLine("Team1");
                if (color2 == "blue") Console.WriteLine("Team2");
                if (color2 == "brown") Console.WriteLine("Team3");
            }
            if (color1 == "red")
            {
                if (color2 == "blue")
                {
                    Console.WriteLine("Team4");
                }
                if (color2 == "black")
                {
                    Console.WriteLine("Team5");
                }
            }
            if (color1 == "green" && color2 =="red")
            {
                Console.WriteLine("Team6");
            }
            /*
             *  if (color1 == "white" && color2 == "green" || color2 == "white" && color1 == "green") Console.WriteLine("Team1");
             *  if (color1 == "white" && color2 == "blue" || color2 == "white" && color1 == "blue") Console.WriteLine("Team2");
             *  if (color1 == "white" && color2 == "brown" || color2 == "white" && color1 == "brown") Console.WriteLine("Team3");
             *  if (color1 == "red" && color2 == "blue" || color2 == "red" && color1 == "blue") Console.WriteLine("Team4");
             *  if (color1 == "red" && color2 == "black" || color2 == "red" && color1 == "black") Console.WriteLine("Team5");
             *  if (color1 == "red" && color2 == "green" || color2 == "red" && color1 == "green") Console.WriteLine("Team6");
             *  if (color1 != "red" || color1 != "white" || color1 != "green" || color1 != "black" || color1 != "blue" || color1 != "brown") Console.WriteLine("ERROR");
             */
            if (color1 != "red" && color1 != "white")
            {
                Console.WriteLine("ERROR");
            }
            Console.ReadLine();
        }

    }
}

